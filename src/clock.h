#ifndef __CLOCK_H__
#define __CLOCK_H__

typedef enum {
	CLOCK_HORIZONTAL_POSITION_LEFT,
	CLOCK_HORIZONTAL_POSITION_CENTER,
	CLOCK_HORIZONTAL_POSITION_RIGHT,
} ClockHorizontalPosition;

typedef struct {
	int line;
	ClockHorizontalPosition pos;
	bool show_seconds;
} ClockConfig;

void* show_clock(void *_config);
ClockConfig *clock_config_new(int line, ClockHorizontalPosition pos, bool show_seconds);
#endif
