#include <time.h>
#include <stdbool.h>
#include <stdio.h>
#include <pigpio.h>
#include <stdlib.h>
#include "i2c_lcd.h"
#include "clock.h"

/**
 * Display a HH:MM clock
 */
void* show_clock(void *_config) {
	static int hour = -1, min = -1, sec = -1;
	int len, pad;
	time_t rawtime;
	struct tm *timeinfo;
	char clockbuf[17];
	ClockConfig *config = _config;

	// Calculate padding for horizontal positioning
	len = (config->show_seconds) ? 8 : 5;

	switch (config->pos) {
		case CLOCK_HORIZONTAL_POSITION_LEFT:
			pad = 0;
			break;
		case CLOCK_HORIZONTAL_POSITION_CENTER:
			pad = (16 - len) / 2;
			break;
		case CLOCK_HORIZONTAL_POSITION_RIGHT:
			pad = 16 - len;
			break;
	}


	while (true) {

		// Get the current time
		time(&rawtime);
		timeinfo = localtime(&rawtime);

		
		if (	// Check if it is different from the last time
				(hour != (int)timeinfo->tm_hour || min != (int)timeinfo->tm_min) || 
				// or option show_seconds is set, in which case we need to update every second
				config->show_seconds == true
				) {
			hour = (int)timeinfo->tm_hour;
			min = (int)timeinfo->tm_min;

			if (config->show_seconds) {
				sec = (int)timeinfo->tm_sec;
				snprintf(clockbuf, sizeof(clockbuf), "%*s%02u:%02u:%02u%*s", pad, "", hour, min, sec, pad, "");
			}
			else {
				snprintf(clockbuf, sizeof(clockbuf), "%*s%02u:%02u%*s", pad, "", hour, min, pad, "");
			}

			// Update the LCD
			lcd_puts(config->line, clockbuf);
		}

		// Sleep 1 second
		gpioSleep(PI_TIME_RELATIVE, 1, 0);
	}
}



ClockConfig *clock_config_new(int line, ClockHorizontalPosition pos, bool show_seconds) {
	ClockConfig *c = malloc(sizeof(ClockConfig));
	c->line = line;
	c->pos = pos;
	c->show_seconds = show_seconds;
	return c;
}

