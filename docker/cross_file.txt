[host_machine]
system = 'linux'
cpu_family = 'arm'
cpu = 'arm7v2'
endian = 'little'

[properties]
c_args = []
c_link_args = []

[binaries]
c = 'armv6-rpi-linux-gnueabihf-gcc'
cpp = 'armv6-rpi-linux-gnueabihf-g++'
ar = 'armv6-rpi-linux-gnueabihf-ar'
ld = 'armv6-rpi-linux-gnueabihf-ld'
objcopy = 'armv6-rpi-linux-gnueabihf-objcopy'
strip = 'armv6-rpi-linux-gnueabihf-strip'
pkgconfig = ''
windres = ''
